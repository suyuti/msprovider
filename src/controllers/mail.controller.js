const msService = require('../services/ms.service')

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET

exports.sendMail = async (mailMessage) => {
    try {
        let loginResp = await msService.login()
        if (loginResp.status != 200) {
            return
        }

        console.log(mailMessage)

        var body = {
            message: {
                subject: mailMessage.subject,
                body: {
                    contentType: 'HTML',
                    content: mailMessage.content
                },
                toRecipients: mailMessage.to.map(t => {
                    return {
                        emailAddress: {
                            address: t
                        }
                    }
                })
            },
            saveToSentItems: 'true'
        }

        if (mailMessage.attachments) {
            body.message.attachments = [ // TODO birden fazla attachment olabilir. Simdilik bir tane yapildi.
                {
                    '@odata.type': "#microsoft.graph.fileAttachment",
                    "name": mailMessage.attachments[0].name,
                    "contentBytes": mailMessage.attachments[0].content
                }
            ]
        }

        let mail = {
            from: mailMessage.from,
            body: body
        }

        let mailResp = await msService.sendMail(mail, loginResp.data.access_token)
    }
    catch (e) {
        console.log(e)
    }
}