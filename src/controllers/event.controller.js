const moment = require('moment')
const msService = require('../services/ms.service')
const ToplantiModel = require('../models/toplanti.model')

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET

exports.sendEvent = async (eventMessage) => {
  try {
    let loginResp = await msService.login()
    if (loginResp.status != 200) {
      return
    }

    if (eventMessage.type == 'create') {
      let event = {
        subject: eventMessage.subject,
        body: {
          contentType: "HTML",
          content: eventMessage.content,
        },
        start: {
          dateTime: moment(eventMessage.start).toISOString(), //"2019-03-15T12:00:00",
          //dateTime: "2019-03-15T12:00:00",
          timeZone: "Turkey Standard Time",
        },
        end: {
          dateTime: moment(eventMessage.end).toISOString(), //"2019-03-15T14:00:00",
          //dateTime: "2019-03-15T14:00:00",
          timeZone: "Turkey Standard Time",
        },
        location: {
          displayName: eventMessage.location
        },
        isOnlineMeeting: eventMessage.isOnline,
        onlineMeetingProvider: "teamsForBusiness",
        attendees: eventMessage.attendees.map(a => {
          return {
            emailAddress: {
              address: a.email,
              name: a.name
            },
            type: 'required'
          }
        })
      }

      let eventResp = await msService.createEvent(event, eventMessage.from, loginResp.data.access_token)
      let toplanti = await ToplantiModel.findByIdAndUpdate(eventMessage._id, { msEventId: eventResp.data.id }, { new: true })
      return { toplanti }
    }
    else if (eventMessage.type == 'update') {
      let event = {
        subject: eventMessage.subject,
        body: {
          contentType: "HTML",
          content: eventMessage.content,
        },
        start: {
          dateTime: moment(eventMessage.start).toISOString(), //"2019-03-15T12:00:00",
          //dateTime: "2019-03-15T12:00:00",
          timeZone: "Turkey Standard Time",
        },
        end: {
          dateTime: moment(eventMessage.end).toISOString(), //"2019-03-15T14:00:00",
          //dateTime: "2019-03-15T14:00:00",
          timeZone: "Turkey Standard Time",
        },
        location: {
          displayName: eventMessage.location
        },
        isOnlineMeeting: eventMessage.isOnline,
        onlineMeetingProvider: "teamsForBusiness",
        attendees: eventMessage.attendees.map(a => {
          return {
            emailAddress: {
              address: a.email,
              name: a.name
            },
            type: 'required'
          }
        })
      }

      let eventResp = await msService.updateEvent(event, eventMessage.toplantiId, eventMessage.from, loginResp.data.access_token)
      let toplanti = await ToplantiModel.findByIdAndUpdate(eventMessage._id, { msEventId: eventResp.data.id }, { new: true })
      return { toplanti }
    }
    else if (eventMessage.type == 'cancel') {

    }

  }
  catch (e) {
    console.log(e)
  }
}