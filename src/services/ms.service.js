const axios = require('axios')
var querystring = require("querystring");

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET


exports.login = async () => {
    let loginResp = await axios.post(
        `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
        querystring.stringify({
            client_id: clientId,
            scope: "https://graph.microsoft.com/.default",
            client_secret: clientSecret,
            grant_type: "client_credentials",
        }),
        {
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
    )
    return loginResp
}


exports.sendMail = async (mail, token) => {
    let mailResp = await axios.post(`https://graph.microsoft.com/v1.0/users/${mail.from}/sendMail`,
        mail.body,
        { headers: { Authorization: "Bearer " + token } }
    )
    if (mailResp.status == 202) {
        console.log('mail gonderildi')
    }
    return mailResp
}

exports.createEvent = async (event, from, token) => {
    try {
          let mailResp = await axios.post(`https://graph.microsoft.com/v1.0/users/${from}/calendar/events`,
            event,
            {
                headers: {
                    //'Content-type': 'application/json',
                    Authorization: "Bearer " + token
                }
            }
        )
        if (mailResp.status == 201) {

            // TODO toplantiID guncellenecek

            console.log('event olusturuldu')
        }
        return mailResp
    }
    catch (e) {
        console.log(e)
        throw e
    }
}

exports.updateEvent = async (event, eventId, from, token) => {
    try {
          let mailResp = await axios.patch(`https://graph.microsoft.com/v1.0/users/${from}/events/${eventId}`,
            event,
            {
                headers: {
                    //'Content-type': 'application/json',
                    Authorization: "Bearer " + token
                }
            }
        )
        if (mailResp.status == 200) {

            // TODO toplantiID guncellenecek

            console.log('event guncellendi')
        }
        return mailResp
    }
    catch (e) {
        console.log(e)
        throw e
    }
}